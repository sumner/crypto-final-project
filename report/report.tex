% vim: spell spelllang=en_us
\documentclass[conference,10pt]{IEEEtran}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{csquotes}
\usepackage[backend=biber]{biblatex}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{listings}
\usepackage{minted}
\usepackage{tipa}
\usepackage{mdframed}
\usepackage[inline]{enumitem}
\usepackage{environ}
\usepackage{varioref}

\addbibresource{../references.bib}

\setminted{autogobble,python3,mathescape,frame=lines,framesep=2mm,fontsize=\footnotesize,escapeinside=||}

\NewEnviron{equations}{%
\begin{equation}\begin{split}
    \BODY
\end{split}\end{equation}
}

\usepackage{pgfplots}
\pgfplotsset{compat=newest}

\theoremstyle{definition}
\newtheorem{definition}{Definition}

\newcommand{\F}{\mathbb{F}}
\newcommand{\op}{\circ}

\title{%
    Analysis of Elliptic Curve Cryptography and Its Relative Advantages and
    Disadvantages Over Alternative Public Key Cryptosystems
}
\author{%
    \IEEEauthorblockN{%
        Tanner Coggins\IEEEauthorrefmark{1},
        Jonathan Sumner Evans\IEEEauthorrefmark{2},
        Joshua Hallinan\IEEEauthorrefmark{3}, and
        David Florness\IEEEauthorrefmark{4}
    }
    \IEEEauthorblockA{%
        Department of Computer Science,
        Colorado School of Mines\\
        Golden, Colorado\\
        Email:
            \IEEEauthorrefmark{1}tcoggins@mines.edu
            \IEEEauthorrefmark{2}jonathanevans@mines.edu,
            \IEEEauthorrefmark{3}jhallinan@mines.edu
            \IEEEauthorrefmark{4}davidflorness@mines.edu
    }
}

\begin{document}
\maketitle
\thispagestyle{plain}
\pagestyle{plain}

% (1) an abstract
\begin{abstract}
    The mathematical study of elliptic curves has given rise to a new means of
    performing public-key cryptography, including the ability to conduct key
    agreements, to generate digital signatures, and to encrypt messages for
    confidentiality. Elliptic Curve Cryptography (ECC) was initially motivated
    by concerns over the future of the Rivest-Shamir-Adleman (RSA) cryptosystem;
    RSA relies on the difficulty of prime factorization, but specialized
    algorithms have appeared that tackle the problem of prime factorization and
    as a consequence RSA keys now need to be very long to protect against
    brute-force attacks. These large key lengths come with the unfortunate
    consequence that low-end devices like mobile phones can no longer easily use
    RSA to perform cryptography. Keys generated using ECC, on the other hand,
    need not be as large since the difficulty of the trapdoor behind ECC, the
    Elliptic Curve Discrete Logarithm problem, is believed to be harder than
    prime factorization. In this paper, we shall discuss the mathematical theory
    behind elliptic curves, how they can be used to perform the aforementioned
    cryptographic capabilities, and observe the performance difference between
    generating Diffie-Hellman key pairs and Elliptic Curve Diffie-Hellman key
    pairs on an Android device.
\end{abstract}

% (2) introduction (including the background, motivation, and goal)
\section{Introduction}

Public key cryptography, also known as \textit{asymmetric} cryptography, is an
essential component of modern communications. It allows for two or more parties
to communicate securely over the inherently insecure network that is the
internet by using public/private key pairs such that each public key can be
known to everyone while the corresponding private key is only known by its
owner. One of the primary uses of public key cryptography is in key exchange
algorithms whereby two parties exchange a symmetric session key using an
asymmetric key. Because of its very practical use cases, public key cryptography
has been heavily studied and developed by both cryptographers and cryptanalysts.

All public key cryptosystems use a mathematical trapdoor, a one-way function
that is easy to compute in the forward direction but infeasible to reverse
unless supplied with additional information. This additional information is the
\textit{private key} in these public key cryptography systems.

The most popular cryptosystem for public key cryptography in use today is the
Rivest-Shamir-Adleman (RSA) cryptosystem. The trapdoor for RSA derives its
difficulty from the fact that factoring the product of two large prime numbers
is believed to be difficult. As of the time of writing, there is no known way to
easily factor a semiprime\footnote{a natural number that is the product of two
    prime numbers} into its two prime factors given a large enough semiprime.
The belief that no such algorithm exists is known as the \textit{RSA Assumption}
and RSA's security relies on it. Although as of yet there is no easy way to
factor a semiprime into its two prime factors, several specialized algorithms
including the Quadratic Sieve~\cite{heninger2012mining} and the General Number
Field Sieve~\cite{buchmann1993implementation} were created specifically to
tackle prime factorization and have experienced moderate success.

Because of the potential vulnerabilities of the RSA trapdoor, many alternatives
to RSA have been researched, implemented, and deployed. One of the main
alternatives is \textbf{elliptic curve cryptography}. Elliptic curve
cryptography (ECC) is based on the behavior of elliptic curves over finite
fields such as $\F_p$, where $p$ is prime.

In the next subsections, we rigorously describe the prerequisite abstract
algebra required to comprehend elliptic curve cryptography. In
Section~\ref{sec:using-for-crypto} we then describe how elliptic curves can be
used to create robust cryptosystems. We analyze the level to which several
cryptographic libraries provide ECC support in
Section~\ref{sec:analysis-software}, and in Section~\ref{sec:comparison-android}
we describe an experiment to emperically test the efficiency of ECC key
generation using the cryptographic library provided on the Android platform. In
Section~\ref{sec:security-analysis} we analyze the security advantages and
disadvantages of ECC before concluding in Section~\ref{sec:conclusion}.

\subsection{Overview of Elliptic Curves}

We begin by rigorously defining an elliptic curve.

\begin{mdframed}
    \begin{definition}[Elliptic Curve]
        \label{def:elliptic-curve}
        An elliptic curve $E$ is the set of solutions to a Weierstrass equation
        \begin{equation}
            \label{eqn:weierstrass}
            E : Y^2 = X^3 + AX + B,
        \end{equation}
        together with an extra point $\mathcal{O}$, where constants $A$ and $B$
        must satisfy $4A^3 + 27B^2 \neq 0$.
    \end{definition}
\end{mdframed}

The $\mathcal{O}$ point has a $y$-coordinate of $\infty$, and can be thought of
as being directly above every single point on the Cartesian coordinate plane.
Figure \ref{fig:curves} from \cite{alma997232367502341} shows two examples of
elliptic curves given different constants.
\begin{figure}[H]
    \includegraphics[width=\columnwidth]{../graphics/ECs}
    \caption{Two example curves with different constants for $A$ and $B$.}
    \label{fig:curves}
\end{figure}

Two key geometric properties of all elliptic curves are observable in the
examples in Figure \ref{fig:curves}:
\begin{enumerate}
    \item The curves are symmetric over the $x$-axis.
    \item Given a non-vertical line that crosses over two points, the line is
        guaranteed to intersect the curve a third time on another point in the
        graph.
\end{enumerate}
These two properties are critical for defining the addition operator between two
points on an elliptic curve, denoted $\oplus$.
\begin{mdframed}
    \begin{definition}[Addition on an Elliptic Curve]
        \label{def:addition}
        Consider two arbitrary points, $P$ and $Q$ on the curve, $E$. Let $L$ be
        the line defined by $P$ and $Q$, and let $R$ be the third intersection
        of $L$ on $E$. Now consider $R'$, the reflection of $R$ across the
        $x$-axis.

        We say that $P \oplus Q = R'$.
    \end{definition}
\end{mdframed}
The above definition is, in fact, rigorous because $P$ and $Q$ are arbitrary
points on the curve.

Figure \ref{fig:addition} shows the relationship of $P$, $Q$, $L$, $R$, and $R'$
on the curve $E$.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\columnwidth]{../graphics/ECline}
    \caption{Example of addition of $P$ and $Q$ on an elliptic curve $E$.}
    \label{fig:addition}
\end{figure}

\subsection{Definition of a Finite Field}

Elliptic curve cryptography is based on the behavior of an elliptic curve mapped
over a finite field. To understand a finite field, we must first define a group.
\begin{mdframed}
    \begin{definition}[Group]
        \label{def:group}
        A group, $\{G, \op\}$ is a set of elements, $G$, and a binary operation,
        $\op$, which exhibits the following properties:
        \begin{itemize}
            \item closure: $a, b \in \F\ \implies\ (a \op b) \in \F$
            \item associativity: $a \op (b \op c) = (a \op b) \op c$
            \item identity element ($e$) exists: $a \op e = e \op a = a$
            \item inverses exist: $a \op a^{-1} = a^{-1} \op a = e, a^{-1} \in
                \F$.
        \end{itemize}

        A group is \textit{commutative} if it also exhibits the commutative
        property: $a \op b = b \op a$.
    \end{definition}
\end{mdframed}

A finite field is a special case of a group where we have another operation
(usually denoted $\times$), and additional properties. Below is the definition
of a finite field.
\begin{mdframed}
    \begin{definition}[Finite Field]
        \label{def:finite-field}
        A finite field, $\{\mathcal{F}, +, \times\}$, is a set of elements with
        two binary operations (usually referred to as addition and
        multiplication) which exhibits the following properties for both
        operations:
        \begin{itemize}
            \item closure: $a, b \in \mathcal{F}\ \implies\ (a \op b) \in
                \mathcal{F}$
            \item associativity: $a \op (b \op c) = (a \op b) \op c$
            \item identity element ($e$) exists: $a \op e = e \op a = a$
            \item inverses exist: $a \op a^{-1} = a^{-1} \op a = e, a^{-1} \in
                \mathcal{F}$
            \item commutativity: $a \op b = b \op a$
        \end{itemize}
        Further, fields exhibit the distributive property over addition:
        \[
            a \times (b + c) = (a \times b) + (a \times c)
        \]
        and have no zero divisors: $ab = 0\ \implies\ a = 0 \vee b = 0$.
    \end{definition}
\end{mdframed}

The order of a finite field $\F$ (that is, the number of elements in its
corresponding set $\mathcal{F}$) is always a power of a prime (although standard
elliptic curves often use fields whose order is simply a prime) and all finite
fields of the same order are isomorphic (that is, have the same algebraic
structure). Thus, it is convention to use $\F_p$ or $GF(p)$ to denote a field of
order $p$. We use the $\F_p$ convention in this paper.

\begin{mdframed}
    \textbf{A note on semantics:} When considering an element $X$ in a group or
    field, and an integer $n$, we use the following convention to denote
    repeated application of the \textit{addition} operator:
    \begin{equation*}
        nX = \underbrace{X + X + \cdots + X}_{n}.
    \end{equation*}
    This is different than elementary algebra where $nX$ usually denotes
    multiplication.
\end{mdframed}

\subsection{Mapping an Elliptic Curve Over a Finite Field}
\label{sec:mapping-over-finite-field}

Until now, we have been describing elliptic curves in a continuous Cartesian
plane. This presents a problem for cryptography, where we need \textit{discrete}
values. Thus, in order to utilize elliptic curves for cryptography, we must map
them to a discrete space. Specifically, we map the curve over the finite field
$\F_p$.

We perform this mapping by finding the set of points that lie on $E$ and whose
coordinates are integers in $\F_p$. This discrete, finite subset of the points
in $E$ forms a commutative group under elliptic curve addition. This group is
denoted $E(\F_p)$.

It should be clear that only a few elliptic curves will intersect with
convenient integer values. Because of this, only a few curves are used by
elliptic curve cryptosystems. We present further discussion of these curves in
Section \ref{sec:ecc-curve-parameters}.

\section{Using Elliptic Curves for Cryptography}
\label{sec:using-for-crypto}

In this section, we describe how the elliptic curves that we discussed in the
previous section can be used to create robust cryptosystems. Essential to every
cryptosystem is a trapdoor, which we describe in Section~\ref{sec:ecc-trapdoor}.
In Section~\ref{sec:ecc-curve-parameters}, we describe the standard parameters
used by all elliptic curve implementations. We then describe cryptosystems that
use elliptic curves for key agreement (\ref{sec:key-agreement}), digital
signatures (\ref{sec:digital-signatures}), and for encryption
(\ref{sec:encryption-decryption}).

\subsection{Cryptographic Trapdoor for ECC}
\label{sec:ecc-trapdoor}

The trapdoor for ECC is the difficulty of determining how many elliptic curve
additions were performed on a given point $P$ to get a resulting point $Q_A =
nP$. In ECC cryptosystems, $n$ is the private key. We present a further
discussion of the security of this trapdoor in Section
\ref{sec:trapdoor-difficulty}.

\subsection{Elliptic Curve Parameters}
\label{sec:ecc-curve-parameters}

As mentioned in Section \ref{sec:mapping-over-finite-field}, due to the
difficulty of finding curves which conveniently map over a finite field, there
are only a few standardized curves. Each of the standardized curves is
associated with a corresponding large prime $p$ which specifies that $\F_p$ is
the field over which the curve is mapped, and a starting point $P \in E(\F_p)$.

The National Institute of Standards and Technology (NIST) has recommended curves
of varying lengths in their Digital Signature Standard (DSS) document
\cite[Appendix D]{nist:dss}. They utilize two different kinds of fields, as described in
that document:
\begin{itemize}
    \item \textit{Prime fields:} the field $GF(p)$. This kind of field requires
        performing integer arithmetic under modulo $p$.
    \item \textit{Binary fields:} the field $GF(2^m)$. This kind of field
        defines its arithmetic in terms of operations on the bit strings which
        represent the elements.
\end{itemize}

For each of the curves over prime fields, NIST specifies a variety of parameters
including the prime modulus $p$, the coefficients of the elliptic curve equation
from Equation \ref{eqn:weierstrass}, and the $x$ and $y$ coordinates of a point
on the curve called the base point. For each of the curves over binary fields,
NIST specifies similar parameters.

The approved curves over prime fields are P-192, P-224, P-256, P-384, and P-521.
The approved curves over binary fields are K-163, B-163, K-233, B-233, K-283,
B-283, K-409, B-409, K-571, and B-571.  The numbers in all of the curves above
represent the key length. P indicates that the curve is over a prime field,
while K and B indicate that the curve is over a binary field. (Specifically, the
K indicates that the curve is a Koblitz curve. We do not have time to discuss
this specific type of curve in our report, and it is not pertinent to our
discussion.)

\subsection{Key Agreement}
\label{sec:key-agreement}

We will now briefly describe the Elliptic Diffie-Hellman Key Exchange (ECDH),
which uses ECC to perform a key exchange very similar to how the ubiquitous
Diffie-Helman Key Exchange does. Suppose Alice and Bob want to share a secret
(for instance, to establish a symmetric key). This is achieved by performing the
following procedure.

\begin{enumerate}
\item A trusted third party chooses a large prime $p$, an elliptic curve $E$
    over $\F_p$, and a point $P \in E(\F_p)$, usually using the parameters
    specified in Section~\ref{sec:ecc-curve-parameters}.
\item Alice and Bob each choose a secret integer; let $n_A$ be Alice's chosen
    integer and $n_B$ be Bob's.
\item Alice computes the point $Q_A$ using elliptic curve arithmetic with $P$ on
    $E(\F_p)$:
    \begin{align*}
      Q_A &= n_A P.
    \end{align*}
    Bob does the same but uses his secret $n_B$ to compute $Q_B$:
    \begin{align*}
      Q_B &= n_B P.
    \end{align*}
\item Alice sends $Q_A$ to Bob and Bob sends $Q_B$ to Alice.
\item Alice can now compute the shared secret $k = n_A Q_B$. Bob can also
    compute $k$ with $k = n_B Q_A$. Since $n_A$ and $n_B$ are only known to
    Alice and Bob, respectively, an adversary Eve observing the exchange cannot
    derive $k$ since Eve only sees $Q_A$ and $Q_B$.
\end{enumerate}

The following equation shows why Alice's derived secret, $k_A$, is the same as
Bob's derived secret, $k_B$:
\[
    k_A = n_A Q_B = n_A (n_B P) = n_B (n_A P) = n_B Q_A = k_B.
\]

\subsection{Digital Signatures}
\label{sec:digital-signatures}

We will now describe the Elliptic Curve Digital Signature Algorithm (ECDSA),
which uses ECC to create signatures for messages that can later be used to prove
the integrity and authenticity of said messages. Suppose Samantha wants to send
a message along with a signature of the message to Victor, who wants to verify
the message's integrity and authenticity using the signature. This is achieved
by performing the following procedure. We use the notation $x(T)$ to signify the
$x$-coordinate of any point $T$ on the elliptic curve.

\begin{enumerate}
\item A trusted third party chooses a finite field $\F_p$, an elliptic curve
    $E(\F_p)$, and a point $P \in E(\F_p)$ of large prime order $q$, usually
    using the parameters specified in Section~\ref{sec:ecc-curve-parameters}.
\item Samantha chooses a secret signing key $1 < s < q-1$. She then uses $s$ to
    compute $V = sP \in E(\F_p)$ and publishes $V$.
\item Samantha takes the message she wants to sign, $d \bmod{q}$, chooses a
    random element $e \in E(\F_p)$ and sends $d$ along with the signature
    $(s_1, s_2)$ to Victor where
    \begin{align*}
      s_1 &= x(eP) \bmod{q} \\
      s_2 &\equiv (d + ss_1)e^{-1} \pmod{q}.
    \end{align*}
\item Victor can then verify the message by computing
    \begin{align*}
      v_1 &\equiv ds_2^{-1} \pmod{q} \\
      v_2 &\equiv s_1s_2^{-1} \pmod{q}
    \end{align*}
    and verifying that
    \begin{align*}
      x(v_1P + v_2V) \bmod{q} = s_1.
    \end{align*}
\end{enumerate}

\subsection{Encryption and Decryption}
\label{sec:encryption-decryption}

We will now describe the process for encrypting and decrypting messages using an
ECC cryptosystem. We describe the steps for the case where Bob wants to send an
encrypted message to Alice.

\begin{enumerate}
    \item Alice must generate a public-private key pair. This is accomplished
        with the following procedure:
        \begin{enumerate}
            \item Alice chooses a private key $n_A$.
            \item Alice computes $Q_A = n_{A}P$ which will be a point in
                $E(\F_p)$.
            \item Alice publishes $Q_A$ as her public key.
        \end{enumerate}

    \item For Bob to encrypt a message to Alice, he will perform the following
        procedure:
        \begin{enumerate}
            \item\label{plaintext} Choose plaintext $M \in E(\F_p)$.
            \item Choose a random number $k$.
            \item Use Alice's public key ($Q_A$) to compute
                \begin{align*}
                    C_1 &= kP \in E(\F_p),\ \ \text{and}\\
                    C_2 &= M + kQ_A \in E(\F_p).
                \end{align*}
            \item Send ciphertext $(C_1, C_2)$ to Alice.
        \end{enumerate}

    \item To decrypt the message from Bob, Alice must reverse the process and
        recover $M$ by computing
        \begin{equation}
            \label{eq:decrypt}
            C_2 - n_AC_1 \in E(\F_p).
        \end{equation}

\end{enumerate}

We will now prove that Equation \ref{eq:decrypt} recovers $M$.
\begin{mdframed}
    \begin{proof}[Proof that Equation \ref{eq:decrypt} recovers $M$]
        Consider Equation \ref{eq:decrypt} and substitute $C_1$ and $C_2$
        \begin{align*}
            C_2 - n_AC_1 &= (M + kQ_A) - n_A(kP).
            \intertext{Since $E(\F_p)$ is a group, we have the associative
                property for both addition and multiplication so we can
                simplify the above expression to}
            &= M + kQ_A - n_AkP. \\
            \intertext{Since $E(\F_p)$ is a commutative group, we can rearrange
                $k$ and $n_A$ to get}
            &= M + kQ_A - kn_AP. \\
            \intertext{Since $n_AP = Q_A$, we can substitute and simplify to get}
            &= M + kQ_A - kQ_A \\
            &= M + e \\
            &= M.
        \end{align*}
        We have now shown that Equation \ref{eq:decrypt} recovers $M$.
    \end{proof}
\end{mdframed}

In principle, this public key cryptosystem works fine. However, in
step~\ref{plaintext} we stated that Bob must choose a plaintext message $M$ that
is on the elliptic curve $E(\F_p)$. There is no obvious way to achieve this if
Bob wants $M$ to be an arbitrary plaintext message\footnote{the Menezes-Vanstone
    cryptosystem is a proposed variant of the Elgamal public key cryptosystem
    that circumvents this issue}. For this and other historical reasons, ECC is
generally not used for encryption. In fact, the NIST curves described in
Section~\ref{sec:ecc-curve-parameters} are only officially to be used for
digital signatures and key exchanges, and thus this is what ECC is predominantly
used for. Encryption using ECC is still very possible, however, but is usually
done indirectly through conducting a key agreement (see
Section~\ref{sec:key-agreement}) and using the shared secret as a symmetric key
for further secure communication between the two parties.

% (3) key idea and approach (e.g., survey, testbed, system
%     design/implementation, experiments, etc.)

% (4) activities (such as literature study and experiments) that you performed
% (5) description and analysis of the key results and observations

\section{Analysis of ECC support in software libraries}
\label{sec:analysis-software}

We now outline the observations we made while observing the usage of ECC in two
popular cryptography libraries: OpenSSL and PyCryptodome.

We begin with OpenSSL, which is heavily used as the core cryptography library on
most Unix-like operating systems. According to the OpenSSL
Wiki~\cite{opensslwikiecc}, the OpenSSL elliptic curve implementation provides
support only for ECDH and for ECDSA. OpenSSL's high-level EVP interface only
supports using ECC keys for signing, verifying, and for key
derivation~\cite{opensslwikievp}.

We now observe the popular PyCryptodome~\cite{pycryptodome} library for the
Python programming language. This library provides a high-level interface to
low-level cryptographic primitives and includes support for ciphers, digital
signatures, hashing, and for the generation of public-private key pairs,
including ones based on RSA, DSA, and ECC. The only elliptic curves that are
supported by PyCryptodome are P-256, P-384 and P-521 which we mentioned in
Section \ref{sec:ecc-curve-parameters}.  Although ECC key pairs can be
generated, the \texttt{Cipher} module of the library, which provides the
functionality for encryption, can only use RSA when conducting assymetric
encryption~\cite{pycryptodomeciphermodule}. ECC key pairs in PyCryptodome can
only be used for digital signatures, but the future plans for the library
include adding support for ECDH~\cite{pycryptodomefutureplans}.

\section{Comparing Performance of Generating ECC Key Pairs vs. Generating Key
    Pairs for Other Public-Key Cryptosystems}
\label{sec:comparison-android}

\begin{table*}
    \centering
    \caption{Equivalent Key Lengths According to the OpenSSL Wiki
        \cite{opensslwikiecc}}
    \label{tab:key-size}
    \begin{tabular}{| l | l | l |}
        \hline
        Symmetric Key Length & ECC Key Length & Standard Asymmetric Key Length  \\
        \hline
        80 bits & 160 bits & 1024 bits \\
        112 bits & 224 bits & 2048 bits \\
        128 bits & 256 bits & 3072 bits \\
        192 bits & 384 bits & 7680 bits \\
        256 bits & 521 bits & 15360 bits \\
        \hline
    \end{tabular}
\end{table*}

To empirically test the efficiency of Elliptical Curve key generation, we
decided to choose an available platform on which devices had restricted
computing power.  We chose mobile phones, more specifically those using the
Android OS. This decision was made based on the widespread use of mobile
devices, and immediate availability of the Android Operating System. For our
test, we used a Samsung Galaxy S7, and programmed using the Kotlin language with
the \texttt{Java.Security} package.

The test we designed compares the performance of generating keys for ECDH to
that of generating keys for normal DH. Our program uses the static
\texttt{KeyPairGenerator.getInstance()} method of the \texttt{Java.Security}
package to create an instance of \texttt{KeyPairGenerator} to generate our keys.
The parameters \texttt{EC} and \texttt{DH} were passed to \texttt{getInstance()}
for Elliptic Curve and Diffie Hellman key generators, respectively. The Elliptic
Curve Key Generator was initialized with the \texttt{secp224r1} parameter which
uses the Standards for Efficient Cryptography Group's 224-bit elliptic curve to
produce keys. The Diffie Hellman key generator was initialized to specify the
production of 2048-bit keys. 2048-bit keys were selected because of their
equivalent key strength to 224-bit elliptic curves. We used a table published by
OpenSSL of key strength comparisons as detailed in Table~\ref{tab:key-size}.

With the \texttt{KeyGenerator} instances initialized, the key exchanges were
performed. For our experiment, we conducted 20 trials of 1000 key exchanges for
both the ECDH and DH. The time taken was measured in milliseconds. The results
of the test are displayed in Table~\ref{tab:diffie}.

\begin{table}[H]
    \caption{ECDH vs DH Exchange Times}
    \label{tab:diffie}
    \begin{tabular}{|c|c|}
        \hline
        \begin{tabular}[c]{@{}l@{}}1000 Diffie-Hellman Exchanges\\ 2048 bit (milliseconds)\end{tabular} & 
        \begin{tabular}[c]{@{}l@{}}1000 ECDH Exchanges\\ 224 bit (milliseconds)\end{tabular}     \\ \cline{1-2} 
        18235 & 4705\\
        18924 & 4668\\
        16943 & 4699\\
        16897 & 4894\\
        16494 & 4688\\
        16863 & 5629\\
        16782 & 5596\\ 
        20769 & 5216\\
        24407 & 5306\\ 
        18509 & 5193\\
        18284 & 5306\\
        18537 & 4800\\
        18265 & 4739\\
        17503 & 4783\\
        18026 & 5162\\
        18611 & 5428\\
        20904 & 5593\\
        22503 & 5745\\
        20151 & 5457\\
        21007 & 5291\\
        \hline
        \textbf{Average: 18930.70 milliseconds} & \textbf{Average: 5144.85
            milliseconds} \\
        \hline
    \end{tabular}
\end{table}

Upon analysis of our results, we discovered that using ECDH was overwhelmingly
more performant than using DH. In our example ECDH was more than 3 times quicker
than DH. This is due to the much smaller key size in ECDH, which significantly
increases performance. On a desktop computer or server where there is ample
computing power, large keys are not difficult to compute. However, on a mobile
device such as the Samsung Galaxy S7 there is a restrictive limit on the CPU
speed, number of CPU cores, and memory size due to device size and power
limitations, making large key computation difficult. Thus the speed difference
found in our experiment is more noticeable on the mobile device than on a
desktop computer. Given this stark time difference, it seems very sensible to
use ECDH rather than DH to allow for lower-end devices to generate keys more
efficiently.

\section{Security Analysis}
\label{sec:security-analysis}

All cryptosystems have tradeoffs, and ECC is no different. In this section, we
present an analysis of the advantages and disadvantages of ECC.

\subsection{Advantages of ECC}

\subsubsection{Performant}

As described by \cite{sinha2013performance}, ECC is more performant than RSA.
Our experiments in Section \ref{sec:comparison-android} also confirmed that ECDH
is more performant than DH. This makes it ideal for use in devices with lower
computing power such as mobile devices, internet of things (IoT) devices,
embedded systems, and many other low-power devices. Additionally, in web
applications, it is critical that each connection is served with as little
computational overhead as possible. Using ECC instead of other systems allows
more clock cycles to be dedicated to serving content rather than negotiating
session keys.

\subsubsection{Trapdoor Difficulty}
\label{sec:trapdoor-difficulty}

As explained in Section \ref{sec:ecc-trapdoor}, the trapdoor for ECC is the
difficulty of determining how many elliptic curve additions were performed on a
given point $P$ to get a resulting point $Q_A = nP$. Solving this trapdoor
requires solving the \textit{elliptic curve discrete logarithm problem}.
\begin{mdframed}
    \begin{definition}[Elliptic Curve Discrete Logarithm Problem (ECDLP)]
        Let $E$ be an elliptic curve over the finite field $\F_p$ and let $P$
        and $Q$ be points in $E(\F_p)$. Find an integer $n$ such that $Q = nP$
        \cite{alma997232367502341}.
    \end{definition}
\end{mdframed}

Currently, the fastest known algorithm to solve ECDLP for the curve $E(\F_p)$
takes $\mathcal{O}(\sqrt{p})$ steps, making it more difficult than the normal
discrete logarithm problem which is the mathematical trapdoor behind DH
\cite{alma997232367502341}.

\subsubsection{Approved by NIST for Key Exchange and Digital Signatures}

As mentioned in Section \ref{sec:ecc-curve-parameters}, 15 curves have been
standardized by NIST. The publication describing these curves is \textit{FIPS
    PUB 186-4}, a publication describing the Digital Signature Standard (DSS)
\cite{nist:dss}.  The Standards for Efficient Cryptography Group (SECG) also
published a set of approved elliptic curves in \textit{SEC 2: Recommended
    Elliptic Curve Domain Parameters} \cite{secg:sec2}.

\subsection{Disadvantages of ECC}

% TODO:
% - Points on a curve are hard to compute and thus curves have to be
%   standardized
% - There is no NIST-approved implementation of public-key

\subsubsection{Quantum Computing Breaks ECC}
ECC is broken under sufficient quantum computing power due to Shor's algorithm
\cite{roetteler2017quantum}. In fact, it is likely that it is easier than RSA to
break under quantum computing.

\subsubsection{Potential Backdoor in Elliptic Curve Cryptography}

Unfortunately, usage of ECC in the United States has long been subject to
suspicion by many due to the potential that it provides a backdoor for the NSA
to eavesdrop on messages encrypted with ECC. The debate is ongoing and
legitimate.

ECC first gained the NSA's recognition of usability in 1995. This support
directly lead to the approval of ECC as a standard for encryption. Eventually,
15 curves were published by NIST, as described in Section
\ref{sec:ecc-curve-parameters}, but the NSA was not happy with how slowly
companies were adopting ECC techniques, so in 2005, they published
recommendations containing only ECC protocols. Eventually, the NSA did give
other recommendations to make their platform more well rounded
\cite{koblitz:menezes:riddle:enigma}.

When Edward Snowden leaked many of the NSA's internal documents to the press in
2013, many people were startled by the extent of the surveillance apparatus of
the U.S.  Government. One of the revelations in the documents was that RSA, a
company with high credibility in the cryptography world, received \$10 million
to use a specific random number generator devised by the NSA. Everyone thought
that this random number generator was not in use due to its slowness, being 1000
times slower than alternatives. RSA making this a standard in their
cryptographic API meant that many companies were possibly under surveillance by
the NSA through a potential backdoor in that random number generator
\cite{koblitz:menezes:riddle:enigma}.

In 2015, however, the NSA advised against transitioning existing systems to ECC
(many companies had not made the transition even though the NSA recommended it
nearly a decade before) citing the impending implementation of quantum computing
encryption standards. Though the Snowden documents revealed a potential backdoor
in vague terms, the opinion of experts in the crypto community was that if the
PRNG that the NSA installed in RSA's library did in fact provide a backdoor,
then experts outside of the agency should be able to reverse-engineer the
backdoor and determine how the NSA was using it. To date, nobody has been able
to determine whether or not this was in fact a backdoor, and that fact has been
cited as strong evidence that the NSA was not actually any farther ahead than
the private or academic communities in breaking ECC
\cite{koblitz:menezes:riddle:enigma}.

The Snowden documents also suggest that the NSA is not very far into discovering
a practical method for using quantum computers to break ECC. Only a very small
amount of their budget is allocated to researching this topic. Experts claim
that there is only a 50\% chance we will have a quantum computer available in
the next 15 years \cite{burges:factoring-as-optimization}. This fact implies
that fears of quantum computing making ECC obsolete are slightly premature, at
least for the next decade. As such, the NSA's recent suggestion to not use ECC
has been somewhat ignored. Indeed, many companies continued using ECC even after
the Snowden document leak. Many are obviously unconvinced that there is anything
malicious or authoritative about the NSA's recommendations regarding the usage
of ECC.

% (6) discussion of the limitations and potential future work
% (7) conclusion
\section{Conclusion}
\label{sec:conclusion}

Over the past month, we have studied the mathematical theory behind elliptic
curves, examined the application of that theory to create robust cryptosystems,
and evaluated real-world usage of elliptic curve cryptography in existing
software packages. Additionally, we have investigated its advantages and
disadvantages over other public-key cryptosystems.

We have found through experimentation that ECC provides many advantages due to
its smaller key sizes which lead to better performance, especially on low-end
devices.

With further time, we would like to investigate the reasons for choosing
specific curves and further explore the implementation details of various
software libraries that support ECC.

% (8) references
{\printbibliography}

\end{document}
