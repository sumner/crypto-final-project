CSCI 474/574 Final Project
==========================

:Group Members: Tanner Coggins <tcoggins@mines.edu>

                Jonathan Sumner Evans <jonathanevans@mines.edu>

                David Florness <davidflorness@mines.edu>

                Josh Hallinan <jhallinan@mines.edu>

Ideas
-----

- Research why SHA-3 is secure
- Merkel Trees
- Quantum-resistant cryptography
- Elliptic curve cryptography
