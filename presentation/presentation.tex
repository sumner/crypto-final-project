\documentclass{lug}

\usepackage{fontawesome}
\usepackage{etoolbox}
\usepackage{textcomp}
\usepackage[nodisplayskipstretch]{setspace}
\usepackage{xspace}
\usepackage{verbatim}
\usepackage{multicol}
\usepackage{soul}
\usepackage{attrib}

\usepackage{amsmath,amssymb,amsthm}

\usepackage[linesnumbered,commentsnumbered,ruled,vlined]{algorithm2e}
\newcommand\mycommfont[1]{\footnotesize\ttfamily\textcolor{blue}{#1}}
\SetCommentSty{mycommfont}
\SetKwComment{tcc}{ \# }{}
\SetKwComment{tcp}{ \# }{}

\usepackage{siunitx}

\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{decorations.pathreplacing,calc,arrows.meta,shapes,graphs}

\usepackage{biblatex}
\bibliography{../references.bib}
% and kill the abominable icon
\setbeamertemplate{bibliography item}{}
% smaller font
\renewcommand\bibfont{\scriptsize}
\setbeamertemplate{frametitle continuation}[from second]

\AtBeginEnvironment{minted}{\singlespacing\fontsize{10}{10}\selectfont}
\usefonttheme{serif}

\makeatletter
\patchcmd{\beamer@sectionintoc}{\vskip1.5em}{\vskip0.5em}{}{}
\makeatother

% Math stuffs
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\lcm}{\text{lcm}}
\newcommand{\Inn}{\text{Inn}}
\newcommand{\Aut}{\text{Aut}}
\newcommand{\Ker}{\text{Ker}\ }
\newcommand{\la}{\langle}
\newcommand{\ra}{\rangle}
\newcommand{\F}{\mathbb{F}}
\newcommand{\op}{\circ}

\newcommand{\yournewcommand}[2]{Something #1, and #2}

\newenvironment{question}[1]{\par\textbf{Question #1.}\par}{}

\newcommand{\pmidg}[1]{\parbox{\widthof{#1}}{#1}}
\newcommand{\splitslide}[4]{
    \noindent
    \begin{minipage}{#1 \textwidth - #2 }
        #3
    \end{minipage}%
    \hspace{ \dimexpr #2 * 2 \relax }%
    \begin{minipage}{\textwidth - #1 \textwidth - #2 }
        #4
    \end{minipage}
}

\newcommand{\frameoutput}[1]{\frame{\colorbox{white}{#1}}}

\newcommand{\tikzmark}[1]{%
\tikz[baseline=-0.55ex,overlay,remember picture] \node[inner sep=0pt,] (#1)
{\vphantom{T}};
}

\newcommand{\braced}[3]{%
    \begin{tikzpicture}[overlay,remember picture]
        \draw [thick,decorate,decoration={brace,raise=1ex,amplitude=4pt},blue] (#2.south west-|T1.south west) -- node[anchor=west,left,xshift=-1.8ex,text=olive]{#3} (#1.north west-|T1.south west);
    \end{tikzpicture}
}

\title{Analysis of Elliptic Curve Cryptography and Its Relative Advantages and
    Disadvantages Over Alternative Public Key Cryptosystems}
\author{Tanner Coggins, Jonathan Sumner Evans, Joshua Hallinan, and David
    Florness}
\institute{\textbf{Colorado School of Mines} \\
    Department of Computer Science \\
    Golden, CO}

\begin{document}

\begin{frame}{Outline}
    \begin{enumerate}
        \item Introduction to Elliptic Curves Over Finite Fields
        \item Using Elliptic Curves Over Finite Fields for Cryptography
        \item Security Analysis of Elliptic Curve Cryptosystems
        \item Using Elliptic Curve Cryptography in Practice
    \end{enumerate}
\end{frame}

\section{Introduction to Elliptic Curves Over Finite Fields}

\begin{frame}{What is an Elliptic Curve?}
    An \textit{elliptic curve} $E$ is the set of solutions to a Weierstrass
    equation
    \begin{align*}
        E : Y^2 = X^3 + AX + B,
    \end{align*}
    together with an extra point $\mathcal{O}$, where the constants $A$ and $B$
    must satisfy $4A^3 + 27B^2 \neq 0$ \cite{alma997232367502341}.
\end{frame}

\begin{frame}[fragile]{What Does it Look Like?}
    \centering
    \includegraphics[width=\textwidth,natwidth=1024,natheight=510]{../graphics/1024px-ECClines-3.svg.png}
    Picture by \citeauthor{ellipticcurvepic} \cite{ellipticcurvepic}.
\end{frame}

\begin{frame}{Refresher on Finite Fields}
    A finite field, $\{\F, +, \times\}$, is a set of elements with two
    operations (addition and multiplication) which has the following properties
    for both operations:
    \begin{itemize}
        \item \textbf{closure:} $a, b \in \F \implies (a \op b) \in \F$
        \item associativity: $a \op (b \op c) = (a \op b) \op c$
        \item identities ($e$): $a \op e = e \op a = a$
        \item inverses: $a \op a^{-1} = a^{-1} \op a = e, a^{-1} \in \F$
        \item commutativity: $a \op b = b \op a$
    \end{itemize}
    Further, we have distributivity: $a \times (b + c) = (a \times b) + (a
    \times c)$ and no zero divisors: $ab = 0 \implies a = 0 \vee b = 0$.

    Note that $\F_p$ is just different notation for $GF(p)$.
\end{frame}

\begin{frame}[allowframebreaks]{The Addition Operator in $E(\F)$}
    To add two points $P$ and $Q$ on an elliptic curve $E$, we
    \begin{enumerate}
    \item Draw a line between $P$ and $Q$
    \item Find the third point $R$ on the line (this will always exist unless
        the line is vertical)
    \item Flip $R$ over the x-axis to get the final result $R'$
    \item $P \oplus Q = R'$
    \end{enumerate}

    \framebreak

    \includegraphics[width=\textwidth,natwidth=1024,natheight=510]{../graphics/ECClines.svg.png}
    Picture by \citeauthor{ellipticcurveaddpic} \cite{ellipticcurveaddpic}.
\end{frame}

\begin{frame}[allowframebreaks]{Mapping an Elliptic Curve Over a Finite Field}
    Suppose we have a curve $E$ and a finite field $\mathbb{F}_p$.
    \begin{itemize}
    \item For cryptography, we want a discrete set of points on the curve to
        make computation fast and consistent.
    \item It turns out that the set of points that lie on $E$ and whose
        coordinates are integers in $\mathbb{F}_p$ form a group under elliptic
        curve addition.
    \item This group is denoted $E(\mathbb{F}_p)$.
    \end{itemize}

    \framebreak

    For example, consider the curve $E : Y^2 = X^3 + 3X + 8$ over the field
    $\mathbb{F}_{13} = \{0,1,2,\dots,11,12\}$. The set of points on
    $E(\mathbb{F}_{13})$ are
    \begin{align*}
      E(\mathbb{F}_{13}) = \{\mathcal{O}, (1,5), (1,8), (2,3), (2,10),
      (9,6), (9,7), (12,2), (12,11)\}.
    \end{align*}

    \framebreak

    \begin{center}
        \includegraphics[width=7cm]{../graphics/point-addition-mod-p.png}
    \end{center}
\end{frame}

\section{Using Elliptic Curves Over Finite Fields for Cryptography (ECC)}

\begin{frame}{Parameters}
    The following parameters are known to everyone and are used in all
    implementations of ECC.

    \begin{itemize}
        \item A large prime $p$,
        \item An elliptic curve $E$ over $\F_p$, and
        \item A starting point $P$ in $E(\F_p)$.
    \end{itemize}

    If Alice wants Bob to send her an encrypted message using ECC, then she will
    \begin{itemize}
        \item Choose a private key $n_A$.
        \item Compute $Q_A = n_AP$ in $E(\F_p)$.
        \item Publish the public key $Q_A$.
    \end{itemize}
\end{frame}

\begin{frame}{Encryption}
    To encrypt the message to Alice, Bob will
    \begin{itemize}
        \item Choose plaintext $M \in E(\F_p)$.
        \item Choose a random element $k$.
        \item Use Alice's public key $Q_A$ to compute $C_1 = kP \in E(\F_p)$ and
            $C_2 = M + kQ_A \in E(\F_p)$.
        \item Send ciphertext $(C_1, C_2)$ to Alice.
    \end{itemize}
\end{frame}

\begin{frame}{Decryption}
    When Alice wants to decrypt the message from Bob, she will recover $M$ by
    computing
    \begin{equation}
        \label{eq:decrypt-M}
        C_2 - n_AC_1 \in E(\F_p).
    \end{equation}

    Why does this work? Recall from the last slide that $C_1 = kP$ and $C_2 = M
    + kQ_A$. Thus, substituting into Equation \ref{eq:decrypt-M}, we get
    \begin{align*}
        (M + kQ_A) - n_A(kP) &= M + kQ_A - n_AkP & \text{associative} \\
        &= M + kQ_A - kn_AP & \text{commutative} \\
        &= M + kQ_A - kQ_A & Q_A = n_AP \\
        &= M.
    \end{align*}
\end{frame}

\section{Security Analysis of Elliptic Curve Cryptosystems}

\begin{frame}{Benefits of ECC}
    Elliptic Curve Cryptography has many advantages including
    \begin{itemize}
        \item It is easy to perform on a low-end device due to the relatively
            simple operations required.
        \item Breaking elliptic curve cryptosystems requires solving the
            \textit{elliptic curve} discrete logarithm problem which is believed
            to be \textit{harder} than the normal discrete logarithm problem we
            discussed in class.
        \item Using ECC is approved for usage in Diffie Hellman key exchanges
            (ECDH) and for DSA digital signatures (ECDSA).
    \end{itemize}
\end{frame}

\begin{frame}{Disadvantages of ECC}
    \begin{itemize}
    \item Points on a curve are hard to compute and thus curves have to be
        standardized
    \item There is no NIST-approved implementation of public-key cryptography
        that uses ECC
    \item Potential NSA backdoor in the Pseudorandom number generator for for
        NIST SP800--90
    \end{itemize}
\end{frame}

\section{Using Elliptic Curve Cryptography in Practice}

\begin{frame}[allowframebreaks]{Comparison of Key Size and Computational Performance to RSA}
    \begin{center}
        \begin{tabular}{| l | l | r| }
            \hline
            & \multicolumn{2}{c|}{\textbf{Key Size}} \\
            \hline
            \textbf{Security Strength} & ECC & RSA/DSA/DH \\
            \hline
            80 bits & 160 bits & 1024 bits \\
            112 bits & 224 bits & 2048 bits \\
            128 bits & 256 bits & 3072 bits \\
            192 bits & 384 bits & 7680 bits \\
            256 bits & 521 bits & 15360 bits \\
            \hline
        \end{tabular}
    \end{center}

    \framebreak

    \begin{minipage}{0.45\textwidth}  
        \begin{table}[]
        \begin{tabular}{l|llll}
        \begin{tabular}[c]{@{}l@{}}1000 Diffie-Hellman Exchanges\\ 2048 bit (milliseconds)\end{tabular} & 
        \begin{tabular}[c]{@{}l@{}}1000 ECDH Exchanges\\ 224 bit (milliseconds)\end{tabular}       \\ \cline{1-2}
        16943 & 4705\\
        16897 & 4668\\
        16494 & 4699\\
        16863 & 4894\\
        16782 & 4688
        \end{tabular}
    \end{table}
    \end{minipage}
\end{frame}

\begin{frame}{Inner Workings of an Existing ECC Implementation}
    \begin{itemize}
    \item We can't use any curve because finding points on a curve quickly is
        difficult
    \item Thus, software libraries like PyCryptodome use standard curves such as
        P-256, P-384 and P-521
    \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]{References}
    % total hack to redefine section to prevent the bibliography from printing a
    % section header
    \renewcommand{\section}[2]{}
    \printbibliography
\end{frame}

\end{document}
% Local Variables:
% TeX-command-extra-options: "-shell-escape"
% End:
